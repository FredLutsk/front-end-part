import MicroModal from 'micromodal';
import { CLASSES } from './_custom';

MicroModal.init({
	onShow: () => {
		CLASSES.body.classList.add('modal-opened', 'no-scroll');
	}, // [1]
	onClose: () => {
		CLASSES.body.classList.remove('modal-opened', 'no-scroll');
	}, // [2]
	// openTrigger: 'data-custom-open', // [3]
	// closeTrigger: 'data-custom-close', // [4]
	openClass: 'is-open', // [5]
	disableScroll: true, // [6]
	disableFocus: false, // [7]
	awaitOpenAnimation: true, // [8]
	awaitCloseAnimation: true, // [9]
	//debugMode: true // [10]
});

if(!!CLASSES.loginSwitch) {
	CLASSES.loginSwitch.addEventListener('click', () => {
		setTimeout(function () {
			document.querySelector('.js-registration').click();
		}, 100);
	})
}

if(!!CLASSES.registerSwitch) {
	CLASSES.registerSwitch.addEventListener('click', () => {
		setTimeout(function () {
			document.querySelector('.js-login').click();
		}, 100);
	})
}

if(!!CLASSES.loginSwitch2) {
	CLASSES.loginSwitch2.addEventListener('click', () => {
		setTimeout(function () {
			document.querySelector('.js-registration').click();
		}, 100);
	})
} 

// MicroModal.show('modal-8');