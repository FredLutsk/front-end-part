import Inputmask from "inputmask";
import { CLASSES } from './_custom';

const input = CLASSES.phoneInput;
const placeholder = input.getAttribute('placeholder');

if (!!placeholder) {
	const phone = new Inputmask(placeholder, {showMaskOnFocus: true});
	phone.mask(input);
}