/*jslint browser, es6 */
/*global window */



require('./_custom');
require('./_slider');
require('./_modal');
require('./_validate');
require('./_inputmask');
require('./_scroll');
