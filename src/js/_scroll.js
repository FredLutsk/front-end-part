
import { CLASSES } from './_custom';
import { media } from './_custom';
import SimpleBar from 'simplebar'; // or "import SimpleBar from 'simplebar';" if you want to use it manually.
import 'simplebar/dist/simplebar.css';

const config = {
	autoHide: false,
}

if (!!CLASSES.searchCustomScroll && window.innerWidth >= 1200) {
	new SimpleBar(CLASSES.searchCustomScroll, config);
}