export const CLASSES = {
	body: document.querySelector('body'),
	html: document.querySelector('html'),
	mobMenu: document.querySelector('.js-mMenu'),
	hamburgerBtn: document.querySelector('.js-hamburger'),
	hamburgerClose: document.querySelector('.js-hamburger-close'),
	mainSlider: document.querySelector('.js-mainSlider'),
	topSlider: document.querySelector('.js-top-slider'),
	newSlider: document.querySelector('.js-new-slider'),
	loginForm: document.querySelector('.js-login-form'),
	submitLogin: document.querySelector('.js-submit-login'),
	loginSwitch: document.querySelector('.js-login-goRegister'),
	loginSwitch2: document.querySelector('.js-login-goRegister2'),
	forgotPass: document.querySelector('.js-forgot-pass-link'),
	registerSwitch: document.querySelector('.js-login-goLogin'),
	phoneInput: document.querySelector('.js-mask'),
	searchField: document.querySelector('.js-search'),
	searchCustomScroll: document.querySelector('.js-customscroll-search'),
	searchResults: document.querySelector('.js-search-results'),
	searchOverlayForMobile: document.querySelector('.js-search-overlay'),
	catalog: document.querySelector('.js-catalog'),
	catalogItems: document.querySelectorAll('.js-catalog-item'),
	catalogMenu: document.querySelectorAll('.js-catalog-menu'),
	minicart: document.querySelector('.js-minicart'),
	minicartDropdown: document.querySelector('.js-minicart-dropdown'),
	searchTrgiggerClass: 'search-active',
	noScroll: 'no-scroll',
	active: 'is-active',
	menuOpened: 'menu-opened',
}

export const media = {
	desktop: window.innerWidth >= 1200,
	notDesktop: window.innerWidth <= 1199,
	tablet: window.innerWidth >= 768 && window.innerWidth <= 1199,
	mobile: window.innerWidth <= 767,
}


/********************\
Hamburger
/********************/
const HAMBURGER = {
	open: () => {
		CLASSES.hamburgerBtn.classList.add('header__hamburger--isactive');
		CLASSES.mobMenu.classList.add('header__mobileBar--isactive');
		CLASSES.html.classList.add(CLASSES.noScroll);
		CLASSES.html.classList.add(CLASSES.menuOpened);
	},
	close: () => {
		CLASSES.hamburgerBtn.classList.remove('header__hamburger--isactive');
		CLASSES.mobMenu.classList.remove('header__mobileBar--isactive');
		CLASSES.html.classList.remove(CLASSES.noScroll);
		CLASSES.html.classList.remove(CLASSES.menuOpened);
	},
}

if (!!CLASSES.hamburgerBtn && !!CLASSES.hamburgerBtn) {
	CLASSES.hamburgerBtn.addEventListener('click', HAMBURGER.open);
	CLASSES.hamburgerClose.addEventListener('click', HAMBURGER.close);
	// document.addEventListener('keydown', HAMBURGER.close);
}

/********************\
Search
/********************/
const searchWindowHandler = (e) => {
	if(!!e.target.closest('.js-search-overlay') === false) {
		CLASSES.html.classList.remove(CLASSES.menuOpened);
		document.removeEventListener('click', searchWindowHandler);
	}
}
/*
Результати пошуку з'являються під час введення тексту, зникають по кліку на область поза пошуком або після видалення текстую
На екрані менше 768 пошук з'являється/зникає під час введення/видалення тексту в полі пошуку
*/

if (!!CLASSES.searchField && window.innerWidth >= 1200) {
	CLASSES.searchField.addEventListener('input', (e) => {
		if (!!e.target.value) {
			CLASSES.searchResults.classList.add(CLASSES.searchTrgiggerClass);
			CLASSES.html.classList.add(CLASSES.menuOpened);

			document.addEventListener('click', searchWindowHandler)
		} else {
			CLASSES.searchResults.classList.remove(CLASSES.searchTrgiggerClass);
		}
	});

	CLASSES.searchField.addEventListener('focusout', (e) => {
		CLASSES.searchResults.classList.remove(CLASSES.searchTrgiggerClass);
	});
} else if (!!CLASSES.searchField && window.innerWidth <= 1199) {
	CLASSES.searchField.addEventListener('input', (e) => {
		if (!!e.target.value) {
			CLASSES.searchOverlayForMobile.classList.add(CLASSES.searchTrgiggerClass);
			CLASSES.html.classList.add(CLASSES.noScroll);
			CLASSES.html.classList.add(CLASSES.menuOpened);
		} else {
			CLASSES.searchOverlayForMobile.classList.remove(CLASSES.searchTrgiggerClass);
			CLASSES.html.classList.remove(CLASSES.noScroll);
			CLASSES.html.classList.remove(CLASSES.menuOpened);
		}
	});
}
/********************\
Catalog
/********************/

if (!!CLASSES.catalogMenu.length && !!CLASSES.catalogItems.length) {
	let menu = [];

	(function getMenueArray() {
		for (let i = 0; i < CLASSES.catalogMenu.length; i++) {
			menu.push(CLASSES.catalogMenu[i]);
		}
	})();

	if (!!CLASSES.catalogItems.length) {
		for (let i = 0; i < CLASSES.catalogItems.length; i++) {
			const el = CLASSES.catalogItems[i];
			el.addEventListener('click', menuHandler);
		}
	}

	function menuHandler(e) {
		e.preventDefault();
		let index = parseInt(e.target.closest('.js-catalog-item').getAttribute('data-click'));

		for (let i = 0; i < menu.length; i++) {
			i === index ? menu[i].classList.add('is-active') : menu[i].classList.remove('is-active');
		}

		window.innerWidth <= 767 ? CLASSES.html.classList.add(CLASSES.noScroll) : '';
		document.addEventListener('click', catalogWindowHandler)
	}

	const catalogWindowHandler = (e) => {
		if (!!e.target.closest('.js-catalog-menu') === false && !!e.target.closest('.js-catalog-item') === false || !!e.target.closest('.js-catalog-close') === true) {
			for (let i = 0; i < menu.length; i++) {
				menu[i].classList.remove('is-active');
			}
			
			CLASSES.html.classList.remove(CLASSES.noScroll);
			document.removeEventListener('click', catalogWindowHandler)
		}
	}
}

/********************\
Minicart
/********************/

const minicartClickHandler = (e) => {
	e.preventDefault();
	CLASSES.minicartDropdown.classList.toggle(CLASSES.active);
	CLASSES.minicart.classList.toggle(CLASSES.active);
	CLASSES.html.classList.toggle(CLASSES.menuOpened);
	document.addEventListener('click', minicartClickWindowHandler)
}

const minicartClickWindowHandler = (e) => {
	if (!!e.target.closest('.js-minicart') === false && !!e.target.closest('.js-minicart-dropdown') === false) {
		CLASSES.minicartDropdown.classList.remove(CLASSES.active);
		CLASSES.minicart.classList.remove(CLASSES.active);
		CLASSES.html.classList.remove(CLASSES.menuOpened);
		document.removeEventListener('click', minicartClickWindowHandler)
	}
}

const minicartFunc = () => {
	if (!!CLASSES.minicart && CLASSES.minicartDropdown && window.innerWidth >= 1200) {
		CLASSES.minicart.addEventListener('click', minicartClickHandler);
	} else {
		CLASSES.minicart.removeEventListener('click', minicartClickHandler);
	}
}

const onWindowResize = {
	init: () => {
		minicartFunc();
	}
}

minicartFunc();

window.addEventListener("resize", onWindowResize.init, false);