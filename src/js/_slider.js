import Swiper, { Navigation, Pagination } from 'swiper';
import { CLASSES } from './_custom';

Swiper.use([Navigation, Pagination]);

var mainSlider = new Swiper(CLASSES.mainSlider, {
	loop: true,
	spaceBetween: 15,
	pagination: {
		el: '.js-mainSlider-pagination',
		clickable: true,
		bulletClass: 'bullet',
		bulletActiveClass: 'bullet--active',
	},
	navigation: {
		nextEl: '.js-mainSlider-next',
		prevEl: '.js-mainSlider-prev',
	},
});

var topSlider = new Swiper(CLASSES.topSlider, {
	loop: false,
	slidesPerView: 1,
	spaceBetween: 10,
	pagination: {
		el: '.js-top-pagination',
		clickable: true,
		bulletClass: 'bullet',
		bulletActiveClass: 'bullet--active',
	},
	navigation: {
		nextEl: '.js-top-next',
		prevEl: '.js-top-prev',
	},
	breakpoints: {
		600: {
			slidesPerView: 2,
		},
		1200: {
			slidesPerView: 4,
			spaceBetween: 30,
			pagination: false,
		},
	},
});

var newSlider = new Swiper(CLASSES.newSlider, {
	loop: false,
	slidesPerView: 1,
	spaceBetween: 10,
	pagination: {
		el: '.js-new-pagination',
		clickable: true,
		bulletClass: 'bullet',
		bulletActiveClass: 'bullet--active',
	},
	navigation: {
		nextEl: '.js-new-next',
		prevEl: '.js-new-prev',
	},
	breakpoints: {
	  600: {
		slidesPerView: 2,
	  },
	  1200: {
		slidesPerView: 4,
		spaceBetween: 30,
		pagination: false,
	  },
	},
});
